import Vue from 'vue'
import Router from 'vue-router'
import home from '../views/home.vue'
import page1 from '../views/page1.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/vue-cli-project-template',
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/page1',
      name: 'page1',
      component: page1
    }
  ]
})
