# vue-cli-project-template

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```

## GitLab Config
```
1.   gitlab server : 127.0.0.1:8929 (pages)

2.   gitlab.rb   (config to enable the gitlab pages )
        ##! Define to enable GitLab Pages
        pages_external_url "http://sg-ai.com"
        gitlab_pages['enable'] = true

```

## Nginx Config 
```
1.  nginx proxy 

    server {
        listen 80;
        server_name ywguo.sg-ai.com;
        location /  {
            proxy_pass http://127.0.0.1:8929/;
            proxy_set_header Host $http_host;
            proxy_set_header X-Forwarded-Proto https;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }
```
